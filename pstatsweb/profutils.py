from __future__ import print_function

import sys
import os
import ast
import marshal
import cProfile
import argparse
from contextlib import contextmanager
from os.path import basename, dirname, abspath, splitext, join, isfile, split

from six import iteritems, itervalues


class FunctionFinder(ast.NodeVisitor):
    """
    This class locates all of the functions and methods in a file and associates any
    method with its corresponding class.
    """
    def __init__(self, fname, cache):
        ast.NodeVisitor.__init__(self)
        self.fname = fname
        self.cache = cache
        self.class_stack = []

    def visit_ClassDef(self, node):
        self.class_stack.append(node.name)
        for bnode in node.body:
            self.visit(bnode)
        self.class_stack.pop()

    def visit_FunctionDef(self, node):
        if self.class_stack:
            qual =  (None, '.'.join(self.class_stack),  node.name)
        else:
            qual = ("<%s:%d>" % (self.fname, node.lineno), None, node.name)

        self.cache[node.lineno] = qual


def find_qualified_name(filename, line, funcname, cache, full=True):
    """
    Determine full function name (class.method) or function for unbound functions.

    Parameters
    ----------
    filename : str
        Name of file containing source code.
    line : int
        Line number within the give file.
    funcname : str
        Name of the function.
    cache : dict
        A dictionary containing infomation by filename.
    full : bool
        If True, assemble the full name else return the parts

    Returns
    -------
    str or None
        Fully qualified function/method name or None.
    """
    if not os.path.isfile(filename):
        return funcname.split()[-1].rstrip('>')

    if filename not in cache and filename.endswith('.py'):
        fcache = {}

        with open(filename, 'r') as f:
            contents = f.read()
            if len(contents) > 0 and contents[-1] != '\n':
                contents += '\n'

            FunctionFinder(filename, fcache).visit(ast.parse(contents, filename))

        cache[filename] = fcache

    try:
        parts = cache[filename][line]
    except KeyError:
        # This can happen for certain python library funcs like those found in abc.py.
        # Not sure why
        return (filename, line, funcname)

    if full:
        if parts[0]:
            return '.'.join((parts[0], parts[2]))
        else:
            return '.'.join((parts[1], parts[2]))

    return parts


def get_prof_and_map(fname):
    """
    Given a filename, read in the augmented profile dump and return the dicts.
    """
    with open(fname, 'rb') as f:
        profdict = marshal.load(f)

    mapfname = fname + '.map'
    if not os.path.isfile(mapfname):
        raise RuntimeError("Cannot find mapping file '%s' needed to compute the diff." %
                           mapfname)

    with open(mapfname, 'rb') as f:
        name_map = marshal.load(f)

    return profdict, name_map


def map_qual_names(profdict):
    """
    Take a profiling data file and find the mapping of qualified names to (fname, line_num)
    """
    mapping = {}
    cache = {}
    for key in profdict:
        fname, line_num, funcname = key
        mapping[find_qualified_name(fname, line_num, funcname, cache, full=True)] = key
    return mapping


def _diff_profile_tups(prof1, prof2):
    """
    Given 2 profile data tuples, return a dict with the difference between them.
    """
    # format of profile dicts:
    #   (filepath, line_num, funcname) : (primitive_calls, total_calls,
    #                                     exclusive_time, inclusive_time, callers)
    #      where callers is a dict of
    #       (filepath, line_num, funcname) : (primcalls_from_caller, total_calls_from_caller,
    #                                         excl_time_in_caller, incl_time_in_caller)
    profdict1, name_map1 = prof1
    profdict2, name_map2 = prof2

    # when doing the diff, line numbers might change, so the full
    # (filename, line_num, funcname) tuples will only catch some of the matches,
    # and there could also be false matches.
    # So we'll convert to qualified names,
    #  (filename, classname or None, funcname) to find any matches and
    #  store both tuples so we know how to index into each dict to perform diffs.
    set1 = set(name_map1)
    set2 = set(name_map2)
    intersection = set1 & set2

    diffdict = {}
    flinefunc2qual = {}
    for key, profkey2 in iteritems(name_map2):
        pcalls2, totcalls2, excl2, incl2, callers2 = profdict2[profkey2]
        if key in name_map1:
            profkey1 = name_map1[key]
            pcalls1, totcalls1, excl1, incl1, callers1 = profdict1[profkey1]
            diffpcalls = pcalls2 - pcalls1
            difftotcalls = totcalls2 - totcalls1
            diffexcl = excl2 - excl1
            diffincl = incl2 - incl1
        else:
            diffpcalls = difftotcalls = 0
            diffexcl = diffincl = 0.
            key = profkey2

        diffdict[profkey2] = (key, diffpcalls, difftotcalls, diffexcl, diffincl,
                              pcalls2, totcalls2, excl2, incl2, callers2)

    return diffdict


sort_types = {
    'calls': 2,
    'cumtime': 4,
}


def diff_profiles(fname1, fname2, limit=100, sorter='cumtime', out_stream=sys.stdout):

    idx = sort_types[sorter]

    prof1, nmap1 = get_prof_and_map(fname1)
    prof2, nmap2 = get_prof_and_map(fname2)

    ttime1 = max(d[3] for d in itervalues(prof1))
    ttime2 = max(d[3] for d in itervalues(prof2))
    totdiff = ttime2 - ttime1

    diffdict = _diff_profile_tups((prof1, nmap1), (prof2, nmap2))
    sorteddiffs = sorted(diffdict.items(), key=lambda x: abs(x[1][idx]), reverse=True)

    try:

        out_stream.write("\n     Total Calls              Total Time (s)            Function\n")
        out_stream.write("   Prof2    Diff        Prof2      Diff      % Diff        Name\n")
        out_stream.write("-------------------------------------------------------------------\n")

        for i, (fline, data) in enumerate(sorteddiffs):
            if i == limit:
                break
            func, diffpcalls, difftotcalls, diffexcl, diffincl, \
                pcalls2, totcalls2, excl2, incl2, callers2 = data
            incl1 = incl2 - diffincl
            pct2 = (incl2/ttime2*100.)
            pct1 = ((incl2-diffincl)/ttime1*100.)
            if abs(incl1) < 1e-20:
                diffpctincl = float('nan')
            else:
                diffpctincl = diffincl / incl1 * 100

            # shorten file names
            if '.py' in func:
                if func.startswith('<'):
                    parts = func[1:].split('>')
                    path = parts[0].split('/')
                    if 'site-packages' in func:
                        path = '/'.join(path[path.index('site-packages')+1:])
                        func = path + parts[-1]

            out_stream.write("%8d (%+7d) %11f (%+8.2f) (%+9.1f%%) %s\n" %
                               (totcalls2, difftotcalls, incl2, diffincl, diffpctincl,
                               func))
    finally:
        if out_stream is not sys.stdout:
            out_stream.close()


def cmd_diff_profiles():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--sort', action='store', dest='sortkey',
                        default='cumtime',
                        help='Determines sort order of profile stat diffs. Options are %s. Sorting '
                        'is performed on the difference in the specified parameter between the two '
                        'runs. Default is cumtime.' % list(sort_types.keys()))
    parser.add_argument('files', metavar='file', nargs=2,
                        help='profile data files to diff.')

    options = parser.parse_args()

    diff_profiles(options.files[0], options.files[1], sorter=options.sortkey)


def get_module_path(fpath):
    """
    Given a module filename, return its full Python module path.

    This includes enclosing packages and is based on existence of ``__init__.py`` files.

    Parameters
    ----------
    fpath : str
        Pathname of file.

    Returns
    -------
    str or None
        Full module path of the given file.  Returns None if the file is not part of a package.
    """
    fpath = abspath(fpath)
    if basename(fpath).startswith('__init__.'):
        pnames = []
    else:
        pnames = [splitext(basename(fpath))[0]]
    path = dirname(fpath)

    initfile = join(path, '__init__.py')
    if not isfile(initfile):
        return None

    while isfile(initfile):
        path, pname = split(path)
        pnames.append(pname)
        initfile = join(path, '__init__.py')

    return '.'.join(pnames[::-1])


def _run_func_from_module(fspec):
    """
    Execute a function specified in fspec, which has the form <modpath>:<testcase>.<func>
    or <modpath>.<func>.
    """
    parts = fspec.split(':')
    if len(parts) != 2:
        raise RuntimeError('function spec must be of the form module:testcase.func or module:func,'
                           ' but you gave "%s"' % fspec)

    modpath, funcpath = parts
    if modpath.endswith('.py'):
        modpath =get_module_path(modpath)

    sys.path.append('.')
    __import__(modpath)
    mod = sys.modules[modpath]

    parts = funcpath.split('.', 1)
    if len(parts) == 2:
        tcase_name, method_name = parts
        testcase = getattr(mod, tcase_name)(methodName=method_name)
        setup = getattr(testcase, 'setUp', None)
        if setup is not None:
            setup()
        getattr(testcase, method_name)()
        teardown = getattr(testcase, 'tearDown', None)
        if teardown:
            teardown()
    else:
        funcname = parts[0]
        getattr(mod, funcname)()


def _finalize(prof, outname):
    prof.disable()
    prof.create_stats()

    profdict = prof.stats
    name_map = map_qual_names(profdict)

    # write out the normal profile data file
    with open(outname, 'wb') as f:
        marshal.dump(profdict, f)

    # now write a file that has the extra
    # mapping information.  It has the same name as the profile dump with
    # a suffix of .map added.
    with open(outname + '.map', 'wb') as f:
        marshal.dump(name_map, f)

def profile_py_file(fname=None):
    """
    Run profiling on the given python script.

    Parameters
    ----------
    fname : str
        Name of the python script.
    """
    if fname is None:
        if '-h' in sys.argv:
            print("usage: save_profile [pyfile] [outname]")
            sys.exit(0)
        args = sys.argv[1:]
        if not args:
            print("No files to process.", file=sys.stderr)
            sys.exit(2)
        fname = args[0]
        if len(args) > 1:
            outname = args[1]
        else:
            outname = 'prof.out'

    if fname.endswith('.py'):
        sys.path.insert(0, os.path.dirname(fname))

        with open(fname, 'rb') as fp:
            code = compile(fp.read(), fname, 'exec')

        globals_dict = {
            '__file__': fname,
            '__name__': '__main__',
            '__package__': None,
            '__cached__': None,
        }

        prof = cProfile.Profile()
        prof.enable()
        try:
            exec (code, globals_dict)
        except KeyboardInterrupt:
            _finalize(prof, outname)
            exit()
    else:  # fname is a modulepath:testcase.funcname or modulepath:funcname
        prof = cProfile.Profile()
        prof.enable()
        try:
            _run_func_from_module(fname)
        except KeyboardInterrupt:
            _finalize(prof, outname)
            exit()

    _finalize(prof, outname)


@contextmanager
def profiling_context(outname='prof.out'):
    """
    Context manager for profiling.

    Parameters
    ----------
    outname : str
        Name of the output file containing profiling data.
    """
    prof = cProfile.Profile()
    prof.enable()

    yield prof

    _finalize(prof, outname)
