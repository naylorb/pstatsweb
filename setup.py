from distutils.core import setup

setup(name='pstatsweb',
      version='0.1.0',
      description="profiling and code analysis framework",
      classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: Apache Software License',
        'Natural Language :: English',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: POSIX :: Linux',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: Implementation :: CPython',
      ],
      keywords='profiling',
      author='Bret Naylor',
      license='Apache License, Version 2.0',
      packages=[
          'pstatsweb',
      ],
      package_data={
          'pstatsweb': ['*.html', '*.js', '*.css', '*.template'],
      },
      install_requires=[
        'six',
        'networkx',
        'tornado',
      ],
      entry_points="""
      [console_scripts]
      view_profile=pstatsweb.pstats_app:cmd_view_pstats
      save_profile=pstatsweb.profutils:profile_py_file
      diff_profiles=pstatsweb.profutils:cmd_diff_profiles
      cProf=cProfile:main
      """
)
